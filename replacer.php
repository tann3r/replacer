<?php

$inumber = 10;
//$jsonobj = array("notnull"); //dirtyhack
$settstack = array();
$archivedir = 'archive';
$logfile = 'log.file';
$settfile = 'prevsett.json';
$info = "INFO: ";

if($_POST["reset"]) {
	unlink($settfile);
}

if($_POST["submit"]) {
	$inumber = $_POST["qinputs"];

	if($_POST["filename"]) {
		$filename = $_POST["filename"];
		$settstack["filename"] = $filename;
	}

	$settstack["qinputs"] = $_POST["qinputs"];


	for ( $i = 0; $i < $inumber; $i++ ) {
		if($_POST["search" . $i]) {
			$settstack["search" . $i] = $_POST["search" . $i];		
		}

		if($_POST["replace" . $i]) {
			$settstack["replace" . $i] = $_POST["replace" . $i];		
		}
	}
	
	$fp = fopen($settfile, "w");

	if ( $fp ) {
		$jsonstr = json_encode($settstack);
		fwrite( $fp, $jsonstr );
	}

	else {
		$info .= "can't create settings file, check dir permissions<br>";
	}

	fclose( $fp );

	if(!file_exists($archivedir)) {
		if(!is_dir($archivedir)) {
			mkdir($archivedir);
		}
	}

	if(file_exists($filename)) {
		if(!is_dir($filename)) {

		$file = file($filename);

		$archivefilename = basename($filename);
		if(!copy( $filename, $archivedir . '/' . $archivefilename . date("-dmy-His") )) {
			$info .= "something went wrong, can't create backup<br>";
		}

		$floghandler = fopen($logfile, "a");

		if(!$floghandler) {
			$info .= "can't open log<br>";
		}

		fwrite($floghandler, "\n" . realpath($filename) . " at " .
						date("H:i:s - d/m/y") . "\n");

		for ( $i = 0; $i < $inumber; $i++ ) {
			if($_POST["search" . $i]) {

	//			$settstack["search" . $i] = $_POST["search" . $i];		
	//			$settstack["replace" . $i] = $_POST["replace" . $i];		

				fwrite($floghandler, "REPLACE QUERY: \"" . $_POST["search" . $i] . "\"" .
							"\t==>\t" . "\"" . $_POST["replace" . $i] . "\"\n" );

				foreach($file as $key=>&$curstr) {
					$tempstr = $curstr;
					$curstr = str_replace($_POST["search" . $i], 
						$_POST["replace" . $i], $curstr);

					if( 0 != strcmp( $tempstr, $curstr )) {
						fwrite($floghandler, "at line " . $key . ": \"" . 
							rtrim($tempstr, "\n") . "\"" .
							"\t==>\t" . "\"" . rtrim($curstr, "\n") ."\"\n");
					}
				}

				unset($curstr);
			}
		}
		
		$fp = fopen($filename, "w");

		if ( $fp ) {
			for ( $i = 0; $i < count($file); $i++ ) {
				fwrite( $fp, $file[$i] );
			}
		}

		fclose( $fp );
		fclose( $floghandler );


		$info .= "SUCCESS. See log.file for more info. (" . date("H:i:s - d/m/y") . ")\n";
		}

		else {
			$info .= "<b>" . $filename . "</b> is directory.<br>";
		}
	}

	else {
		$info .= "no such file. Check filename.<br>";
	}

}

if($_POST["changenentries"]) {
	$inumber = $_POST["qinputs"];

	if($_POST["filename"]) {
		$filename = $_POST["filename"];
		$settstack["filename"] = $filename;
	}

	$settstack["qinputs"] = $_POST["qinputs"];


	for ( $i = 0; $i < $inumber; $i++ ) {
		if($_POST["search" . $i]) {
			$settstack["search" . $i] = $_POST["search" . $i];		
		}

		if($_POST["replace" . $i]) {
			$settstack["replace" . $i] = $_POST["replace" . $i];		
		}
	}

	$fp = fopen($settfile, "w");

	if ( $fp ) {
		$jsonstr = json_encode($settstack);
		fwrite( $fp, $jsonstr );
	}

	fclose( $fp );
}

$fp = fopen($settfile, "r");

if ( $fp ) {
	while( !feof($fp) ) {
		$fstr = fgets($fp);
	}
	
	$jsonobj = json_decode($fstr, TRUE);
}

fclose($fp);

if($jsonobj["qinputs"]) {
	$inumber = $jsonobj["qinputs"];
}

?>

<form method=POST>
	<span>enter filename:</span>
<?php
//if ($jsonobj) {
	echo '<input type="text" name="filename" value="' . $jsonobj["filename"] . '" size="50">'; 
	
?>
<!--	<span>or choose:</span>
	<input type="file" name="filename">-->
	<br>
	<span>number of entries: </span>
<?php
	echo '<input type="text" name="qinputs" value="' . $inumber . '" size="5">';
?>
	<input type="submit" name="changenentries" value="Change">
	<h2>replacing</h2>
<?php 
	
	for ( $j = 0; $j < $inumber; $j++ ) {
		echo 'search: ';
		echo '<input type="text" name="search' . $j . '" value="' . 
				htmlspecialchars($jsonobj["search" . $j], ENT_QUOTES) . '" size="50">'; 
		echo 'replace: ';
		echo '<input type="text" name="replace' . $j . '" value="' . 
				htmlspecialchars($jsonobj["replace" . $j], ENT_QUOTES) . '" size="50">'; 
		echo '<br />';
	}
//}
?>
	<input type="submit" name="submit" value="Replace">
	<input type="submit" name="reset" value="Reset">

	<?php echo $info; ?>
</form>
